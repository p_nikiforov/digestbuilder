JSONDATA = [
{
img:"https://i.imgur.com/QRXEmPy.jpg",
title: "Lorem ipsum dolor sit amet.",
excerpt: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur voluptate incidunt iure minima saepe expedita molestias nam officia ad debitis ratione illum eos quas.",
source: "Lorem"
},
{
img:"https://i.imgur.com/7NHuXsl.jpg",
title: "consectetur adipisicing elit. Iste, blanditiis",
excerpt: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur voluptate incidunt iure minima saepe expedita molestias nam officia ad debitis ratione illum eos quas.",
source: "consectetur"
},
{
img:"https://i.imgur.com/weE26Nr.jpg",
title: "Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur",
excerpt: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur voluptate incidunt iure minima saepe expedita molestias nam officia ad debitis ratione illum eos quas.",
source: "dolor"
},
{
img:"https://i.imgur.com/OV4XIc6.jpg",
title: "illum eos quas",
excerpt: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur voluptate incidunt iure minima saepe expedita molestias nam officia ad debitis ratione illum eos quas.",
source: "ratione"
},
{
img:"https://i.imgur.com/rBmHSJw.jpg",
title: "omnis odit ducimus tenetur voluptate",
excerpt: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste, blanditiis, necessitatibus, omnis odit ducimus tenetur voluptate incidunt iure minima saepe expedita molestias nam officia ad debitis ratione illum eos quas.",
source: "ipsum"
}
]