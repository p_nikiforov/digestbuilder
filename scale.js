

$(function(){



var sliderElem = document.querySelector('.slider');
    var thumbElem = sliderElem.children[0];
    var pic = document.querySelector('.pictureBlockImage img')
    
    
    var MAX_PIC_WIDTH = document.querySelector('.pictureBlockImage img').width;
    var MIN_PIC_WIDTH = 130;
    var MIN_PIC_HEIGHT = 85;
    
    if ()
    
    document.querySelector('.pictureBlockImage img').width = MIN_PIC_WIDTH
    
    
    
    thumbElem.onmousedown = function(e) {
      var thumbCoords = getCoords(thumbElem);
      var shiftX = e.pageX - thumbCoords.left;
      // shiftY здесь не нужен, слайдер двигается только по горизонтали

      var sliderCoords = getCoords(sliderElem);

      document.onmousemove = function(e) {
        //  вычесть координату родителя, т.к. position: relative
        var newLeft = e.pageX - shiftX - sliderCoords.left;

        // курсор ушёл вне слайдера
        if (newLeft < 0) {
          newLeft = 0;
        }
        var rightEdge = sliderElem.offsetWidth - thumbElem.offsetWidth;
        if (newLeft > rightEdge) {
          newLeft = rightEdge;
        }

        thumbElem.style.left = newLeft + 'px';
        
        var percent = newLeft/rightEdge
        var picPercent = percent
        pic.width = MAX_PIC_WIDTH*picPercent+MIN_PIC_WIDTH
        
      }

      document.onmouseup = function() {
        document.onmousemove = document.onmouseup = null;
      };

      return false; // disable selection start (cursor change)
    };

    thumbElem.ondragstart = function() {
      return false;
    };

    function getCoords(elem) { // кроме IE8-
      var box = elem.getBoundingClientRect();

      return {
        top: box.top + pageYOffset,
        left: box.left + pageXOffset
      };

    }

})