Digest = {

  storage: {
    config: {      
      topNews: {
        title: 'Top News',
        image: 1,
        sharing: 1,
        dontShare: 1
      },
      ourWord: {
        title: 'Our Word',
        image: 1,
        sharing: 1,
        dontShare: 0
      },
      worldwide: {
        title: 'Worldwide',
        image: 1,
        sharing: 1,
        dontShare: 0
      },
      competitors: {
        title: 'Competitors',
        image: 0,
        sharing: 0,
        dontShare: 0
      }
    },
    
    data:{
    
    }
  },
  
  make: {
    
    
    block: function(type)
    {
      _type = Digest.storage.config[type]
      
       $('.container').append(
            Digest.make.getTemplate('newsBlock')
                .replace('##title##',_type.title)
                .replace(/##class##/g,_type.title.replace(' ','')
                .replace(_type.title[0],_type.title[0].toLowerCase()))
                .replace('##data##',
                    Digest.make.makeItems(type)
                )
        );
    },
        
    getTemplate: function(type)
    {
      
       return $('template').filter('[data-tpl~="' + type + '"]').html().trim();
      
    },
    
    makeItems: function(type) 
    {
      type = Digest.storage.config[type];
      getTemplate = Digest.make.getTemplate;
      
      item = getTemplate('inputGroup')
      
      replaceImage = type.image ? getTemplate('pictureBlock') : '';
      
      replaceDontShare = type.dontShare ? getTemplate('dontShareButton') : '';
      
      return item.replace('##picture##',replaceImage).replace('##dontshare##',replaceDontShare);
                 
      
    },
    
    addNewItem: function(e){
      
      const target = $(e.currentTarget);
      
      target.closest('.newsBlock').find('.newsBlocks').append(Digest.make.makeItems(target.attr('data-type')))
    
    },
    
    removeItem: function(e){
    
        $(e.currentTarget).closest('.inputGroup').remove()
    
    },
    
    imageBlock: {
    
        clearImage: function(e){
          $(e.currentTarget).attr('src','');
        },
        
        uploadNew: function(e){
          $(e.currentTarget).closest('.image-editor').find('input[type="file"]').click();
        }
    
    }
    
    
  },
  
  init: function() 
  {
  
      for (let key in Digest.storage.config){
          Digest.make.block(key);
      }
      
      $(document)
          .on('click', '.addNewPost', Digest.make.addNewItem)
          .on('click', '.removeButton', Digest.make.removeItem)
          .on('dblclick', '.cropit-preview-image', Digest.make.imageBlock.clearImage)
          .on('click','.uploadImageButton', Digest.make.imageBlock.uploadNew)
          .find('.image-editor').cropit()
  }
  
};

$(function(){
  Digest.init();
});
