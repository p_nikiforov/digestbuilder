$(function() {
    newsBlocks = [{
        name: 'topNews',
        title: 'Top News'
    }, {
        name: 'ourWord',
        title: 'Our Word'
    }, {
        name: 'worldwide',
        title: 'Worldwide'
    }, {
        name: 'competitors',
        title: 'Competitors'
    }]

    var AddNewPost = function() {
        $($(this).prev()).append(buildInputGroup($(this).prev()));
        $('.image-editor', 'body').cropit({
          imageState: {
            src: JSONDATA[imgCounter].img,
          },
        });
        (imgCounter > 3) ? imgCounter = 0 : imgCounter++
       
        
    }

    function appendNewsBlock(name, title, parent) {
        newsBlock = '<div class="newsBlock ##class##"><div class="title"><h2>##title##</h2></div><div class="newsBlocks ui-sortable"></div></div>'
        addNewButton = '<div class="addNewPost">+</div>'
        if (!title) title = name;
        newsBlock = newsBlock.replace('##class##', name);
        newsBlock = newsBlock.replace('##title##', title);
        if (!parent) parent = $('.container');
        $(parent).append(newsBlock);
        $('.' + name).append(addNewButton);
    }


    function buildInputGroup(parent) {
        inputGroup = $('<div class="inputGroup ui-sortable-handle ui-draggable ui-draggable-handle"></div>');
        inputGroup.append(buildInputField('link'));
        if (!parent.parent().hasClass('competitors')) {
            inputGroup.append(buildPictureBlock());
        }
        inputGroup.append(buildInputField('title'));
        inputGroup.append(buildInputField('excerpt', '', 'textarea'));
        inputGroup.append(buildInputField('source'));
        inputGroup.append(buildRemoveButton());
        if (parent.parent().hasClass('topNews')) {
            inputGroup.append(buildDontShareButton());
        }
        return inputGroup;
    }


    function buildInputField(name, title, type) {
        if (!title) title = name;
        if (type == 'textarea') type = 'textarea'
        if (!type) type = 'input type="text"';
        inputField = '<##type## class="##class##" placeholder="##title##">';
        inputField = inputField.replace('##class##', name);
        inputField = inputField.replace('##title##', title);
        inputField = inputField.replace('##type##', type);
        inputField = $(inputField);
        return inputField
    }


    function buildPictureBlock() {
        pictureBlock = $('#pictureBlock').html();
        // pictureBlock = pictureBlock.replace('##id##',id)
        return pictureBlock
    }


    function buildRemoveButton() {
        removeButton = '<div class="removeButton">&#128937;</div>'
        return removeButton;
    }


    function buildDontShareButton() {
        dontShareButton = '<div class="dontShareButton"><label><input type="checkbox" class="dontShareButton">Do not share</label></div>'
        return dontShareButton;
    }


    function buildSharingBlock() {
        sharingBlock = $('<div class="sharingBlock"><h4>Sharing links</h4></div>')
        facebook = buildInputField('facebook', 'there will be facebook link').attr('readonly', 'readonly');
        twitter = buildInputField('twitter', 'there will be twitter link').attr('readonly', 'readonly');
        linkedin = buildInputField('linkedin', 'there will be linkedin link').attr('readonly', 'readonly');
        sharingBlock.append(facebook, twitter, linkedin)
        return sharingBlock;
    }
    
    
    
    for (i = 0; i < newsBlocks.length; i++) {
        appendNewsBlock(newsBlocks[i].name, newsBlocks[i].title);
        
    }
    
    var imgCounter = 0
    
    
    
    
    
    $('body').on('click','.addNewPost', AddNewPost)
    
    $('.addNewPost').click();
    
    $('.cropit-preview-image').dblclick(function()
    {
    $(this).attr('src','')
    })
    
   $('.uploadImageButton', 'body').on('click',  function() {
        $('input[type="file"]', $(this).parent()).click();
    })
    
    
    $('body').on('click', '.removeButton', function() {
        $(this).parent().remove();
    })
    
    $('body').on('keyup', '.inputGroup .link', function() {
        $this = this;
        $(this).parent().parent().addClass('progress');
        link = $(this).val();
        $('#t').load('https://crossorigin.me/' + link + ' meta[name="twitter:title"]');
        $('#d').load('https://crossorigin.me/' + link + ' meta[name*="description"]');
        $('#tw').load('https://crossorigin.me/' + link + ' meta[name*="site"]');
        fn = function() {
            for (i = 1; i < 3; i++) {
                a = $("meta", '#t').attr("content");
                b = $("meta", '#d').attr("content");
                c = $("meta", '#tw').attr("content");
                c = c.replace('@', '');
                c = c[0].toUpperCase() + c.substring(1);
                setTimeout(arguments.callee, 5000);
                $($this).parent().find('.title').val(a)
                $($this).parent().find('.excerpt').val(b)
                $($this).parent().find('.source').val(c)
            }
        }
        setTimeout(fn, 5000);
        setTimeout($(this).parent().parent().removeClass('progress'), 5000);
    })
    
    $('body').on('change keyup keydown paste cut', 'textarea', function() {
        $(this).height(0).height(this.scrollHeight - 10);
    });
    
     
})

