$( function() {

$( ".newsBlocks" ).sortable({
      revert: true,
      cancel: ".pictureBlock, input, textarea"
    });
    $( ".inputGroup" ).draggable({
      connectToSortable: ".newsBlocks",
      revert: "invalid",
      cancel: ".pictureBlock"
    });
    
    
  } );